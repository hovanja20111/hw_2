from rest_framework import serializers
from .models import Book, Author, Genre

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'name', 'author', 'genre')

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'title', 'name', 'is_russian', 'is_alive','birth_date')

class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id', 'title', 'name', 'for_children', 'publish_date')

    # title = serializers.CharField(max_length = 100)
    # name = serializers.CharField(max_length = 400)
    # genre_id = serializers.IntegerField()
    # author_id = serializers.IntegerField()
    #
    #
    # def create(self, validated_data):
    #     return Book.objects.create(**validated_data)
