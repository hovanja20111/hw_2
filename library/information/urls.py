# from django.urls import path, include
# from .views import BookViewSet
#
# urlpatterns = [
#     path('information/', BookViewSet.as_view({'get': 'list'})),
#     path('information/<int:pk>', BookViewSet.as_view({'get': 'retrive'}))
# ]

from rest_framework.routers import DefaultRouter
from .views import BookViewSet, AuthorViewSet, GenreViewSet

router = DefaultRouter()
router.register(r'book', BookViewSet, basename = 'book')
router.register(r'author', AuthorViewSet, basename = 'author')
router.register(r'genre', GenreViewSet, basename = 'genre')

urlpatterns = router.urls
