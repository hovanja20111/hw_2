from django.db import models
from django.utils import timezone

# Create your models here.
class Author(models.Model):
    title = models.CharField(
        verbose_name = 'Автор книги',
        max_length=200
        )
    name = models.TextField()
    surname = models.TextField()

    is_russian = models.BooleanField(default=False)
    is_alive = models.BooleanField(default=False)
    birth_date = models.DateTimeField(
        default = timezone.now,
        null=True,
        blank=True
        )
    def __str__(self):
        return self.title

class Book(models.Model):
    title = models.CharField(
        verbose_name = 'Название книги',
        max_length=200
        )
    name = models.TextField()
    for_children = models.BooleanField(default=False)
    publish_date = models.DateTimeField(
        default = timezone.now,
        null=True,
        blank=True
        )
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE)
    author = models.ForeignKey('Author',on_delete=models.CASCADE)
    def __str__(self):
        return self.title

class Genre(models.Model):
    title = models.CharField(
        verbose_name = 'Литературный жанр',
        max_length=200
        )
    name = models.TextField()
    for_children = models.BooleanField(default=False)
    publish_date = models.DateTimeField(
        default = timezone.now,
        null=True,
        blank=True
        )
    id_genre = models.IntegerField()

    def __str__(self):
        return self.title
